import asyncio
import aiormq
import aiohttp
import json
import sys
import uvloop
from configparser import RawConfigParser as ConfigParser
from urllib.parse import urlparse, parse_qs, urlunparse, urlencode


def get_bads():
    for i in sys.stdin:
        i = i.strip()
        if i == "":
            continue
        yield i


class Filter:
    def __init__(self, bads):
        self.bads = list(bads)
        print(self.bads)

    def good(self, url):
        for bad in self.bads:
            if bad in url:
                print("Reject {} matches {}".format(url, bad))
                return False
        return True

    async def source_scrub(self, url):
        async with aiohttp.ClientSession() as session:
            async with session.head(url, allow_redirects=True) as r:
                return str(r.url)

    def track_scrub(self, url):
        p = urlparse(url)
        q = parse_qs(p.query)
        newq = {}
        for k in q:
            if "utm_" not in k and "cmp" not in k and "feedType" not in k and "feedName" not in k:
                newq[k] = q[k]
        return urlunparse((p.scheme, p.netloc, p.path, p.params, urlencode(newq), p.fragment))

    async def scrub(self, url):
        u = await self.source_scrub(url)
        return self.track_scrub(u)

    async def connect(self, amqp_url):
        self.connection = await aiormq.connect(amqp_url)
        self.channel = await self.connection.channel()
        await self.channel.basic_qos(prefetch_count=1)

    async def on_message(self, message: aiormq.types.DeliveredMessage):
        u = message.body.decode("utf8")
        url = await self.scrub(u)
        print("`{}` Became `{}`".format(u, url))
        if self.good(url):
            await self.channel.basic_publish(
                exchange="",
                routing_key="news.clean_urls",
                body=json.dumps({"url": url}).encode("utf8"),
                properties=aiormq.spec.Basic.Properties(app_id="newsparser", content_type="application/json"),
            )

    async def done(self):
        await self.channel.close()
        await self.connection.close()


async def stuff():
    Config = ConfigParser()
    Config.read("conf.ini")
    amqp_url = Config.get("AMQP", "url")
    f = Filter(get_bads())
    await f.connect(amqp_url)
    # Declaring queue
    declare_ok = await f.channel.queue_declare("news.new_urls", durable=True)
    # Start listening the queue with name 'task_queue'
    await f.channel.basic_consume(declare_ok.queue, f.on_message, no_ack=True)


def main():
    # Ah! uvloop goes brrr
    uvloop.install()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(stuff())
    loop.run_forever()
