#!/usr/bin/env sh

# Move to docker compose? Better so on kube once we fix Kube

VERSION=3.141

# Prep
docker pull selenium/hub:${VERSION}
docker pull selenium/node-firefox:${VERSION}

# Docker network create grid
docker rm -f selenium-hub
docker run -d -p 4444:4444 --net grid --name selenium-hub selenium/hub:${VERSION}
docker rm -f snode1 snode2
docker run --rm -d --net grid -e HUB_HOST=selenium-hub --name snode1 -v /dev/shm:/dev/shm selenium/node-firefox:${VERSION}
docker run --rm -d --net grid -e HUB_HOST=selenium-hub --name snode2 -v /dev/shm:/dev/shm selenium/node-firefox:${VERSION}
docker rm -f snode3 snode4
docker run --rm -d --net grid -e HUB_HOST=selenium-hub --name snode3 -v /dev/shm:/dev/shm selenium/node-firefox:${VERSION}
docker run --rm -d --net grid -e HUB_HOST=selenium-hub --name snode4 -v /dev/shm:/dev/shm selenium/node-firefox:${VERSION}
