import os
from goose3 import Goose
from selenium import webdriver
from selenium.common.exceptions import (
    UnexpectedAlertPresentException,
    SessionNotCreatedException,
    WebDriverException,
    InvalidSessionIdException,
)
from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer
from sumy.summarizers.lsa import LsaSummarizer
from sumy.nlp.stemmers import Stemmer
from sumy.utils import get_stop_words

os.environ["MOZ_HEADLESS"] = "1"  # Should be moved out
LANGUAGE = "english"  # Should be config option


class NotEnoughContent(Exception):
    def __init__(self, url: str) -> None:
        super().__init__("Not enough content for: {}".format(url))


class InvalidContent(Exception):
    def __init__(self, url: str) -> None:
        super().__init__("Content appears invalid for: {}".format(url))


class Summary:
    def __init__(self, url, title, content):
        self.url = url
        self.title = title
        self.content = content

    def to_dict(self):
        return {"url": self.url, "title": self.title, "summary": self.content}


class Summarizer:
    def __init__(self, language: str = "english", sentence_count: int = 15):
        self.language = language
        self.sentence_count = sentence_count
        self.goose = Goose({"enable_image_fetching": False})
        self.stemmer = Stemmer(language)
        self.tokenizer = Tokenizer(language)
        self.summarizer = LsaSummarizer(self.stemmer)
        self.summarizer.stop_words = get_stop_words(language)

    def parse(self, content) -> (str, str):
        """
        Parse retrieve the given url and parse it.
        :param url: The URL to parse
        :return: The resolved URL, the parsed content
        """
        try:
            contents = self.goose.extract(raw_html=content["content"])
            parser = PlaintextParser.from_string(contents.cleaned_text, self.tokenizer)
            sentences = self.summarizer(parser.document, self.sentence_count)
            if len(sentences) < self.sentence_count:
                return {"url": content["url"], "error": "Not enough content"}
        except Exception as e:
            return {"url": content["url"], "error": str(e)}
        return {
            "url": content["url"],
            "title": content["title"],
            "summary": " ".join(str(sentence) for sentence in sentences),
        }
