import os
from selenium import webdriver
from selenium.common.exceptions import (
    UnexpectedAlertPresentException,
    SessionNotCreatedException,
    WebDriverException,
    InvalidSessionIdException,
    TimeoutException
)


class BrowserOutput:
    def __init__(self, url, title, content):
        self.url = url
        self.title = title
        self.content = content

    def to_dict(self):
        return {"url": self.url, "title": self.title, "content": self.content}


class NotEnoughContent(Exception):
    def __init__(self, url: str) -> None:
        super().__init__("Not enough content for: {}".format(url))


class InvalidContent(Exception):
    def __init__(self, url: str) -> None:
        super().__init__("Content appears invalid for: {}".format(url))


class Browser(object):
    def __init__(
        self,
        grid_url: str = "http://localhost:4444/wd/hub",
        req_browser: str = "firefox",
        javascriptEnabled: bool = True,
    ) -> None:
        self.browser = None
        self.grid_url = grid_url
        self.req_browser = req_browser
        self.javascriptEnabled = javascriptEnabled

    def init(self) -> None:
        if self.browser:
            self.done()
        for i in range(1,10):
            try:
                self.browser = webdriver.Remote(
                    command_executor=self.grid_url,
                    desired_capabilities={"browserName": self.req_browser, "javascriptEnabled": self.javascriptEnabled},
                )
                break
            except TimeoutException:
                continue


    def __enter__(self):
        self.init()
        return self

    def __exit__(self, *args):
        self.done()

    def _blank(self):
        """
        Empty browser, do not kill instance
        """
        try:
            self.browser.get("about:blank")
        except UnexpectedAlertPresentException:
            self.browser.switch_to.alert()
            self.browser.switch_to.alert().dismiss()

    def parse_url(self, url: str) -> BrowserOutput:
        """
        Parse retrieve the given url and parse it.
        :param url: The URL to parse
        :return: The resolved URL, the parsed content
        """
        try:
            self.browser.get(url)
        except UnexpectedAlertPresentException:
            self.browser.switch_to.alert()
            self.browser.switch_to.alert().dismiss()
            self.browser.get(url)
        except WebDriverException:
            raise InvalidContent(url)
        try:  # Move around any alerts
            self.browser.switch_to.alert()
            self.browser.switch_to.alert().dismiss()
        except Exception:
            pass
        try:
            cleaned_url = self.browser.current_url
            title = self.browser.title
            contents = self.browser.page_source
        except IndexError:
            raise InvalidContent(url)
        finally:
            self._blank()
        return BrowserOutput(cleaned_url, title, contents)

    def done(self) -> None:
        try:
            self.browser.close()
        except WebDriverException:
            pass
        try:
            self.browser.quit()
        except SessionNotCreatedException:
            pass
        except InvalidSessionIdException:
            pass
        except WebDriverException:
            pass
        self.browser = None
