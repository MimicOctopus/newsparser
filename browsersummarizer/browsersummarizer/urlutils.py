from bs4 import BeautifulSoup


def parse_html_for_urls(body: str) -> [str]:
    urls = set()
    soup = BeautifulSoup(body, "lxml")
    for link in soup.find_all("a", href=True):
        if None is link:
            continue
        a = link["href"]
        urls.add(a)
    return list(urls)
