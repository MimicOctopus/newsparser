#!/usr/bin/env python3

import sys
from browsersummarizer.browser import Browser
from browsersummarizer.summarizer import Summarizer, NotEnoughContent, InvalidContent
import json
from configparser import RawConfigParser as ConfigParser
from resource_pool import Pool
import asyncio
import concurrent.futures
import aiormq
import uvloop


def urls_from_stdin():
    for line in sys.stdin:
        yield line.strip()


class SummaryGenerator:
    """Helps generate summaries"""

    def __init__(self, executor, grid_url, pool_size):
        self.grid_url = grid_url
        self.executor = executor
        self.browsers = Pool(factory=lambda: Browser(self.grid_url), pool_size=pool_size)
        self.summarizers = Pool(factory=lambda: Summarizer(), pool_size=5 * pool_size)

    async def connect(self, url):
        self.connection = await aiormq.connect(url)
        self.channel = await self.connection.channel()

    def do_work(self, url):
        with self.browsers.reserve() as browser:
            with browser as b:
                try:
                    out = b.parse_url(url)
                    if out.content is None:
                        return {"url": url, "error": "No content"}
                    else:
                        return out.to_dict()
                except NotEnoughContent as nec:
                    return {"url": url, "error": "Not enough content"}
                except InvalidContent:
                    return {"url": url, "error": "Invalid content"}
                except Exception as e:
                    return {"url": url, "error": str(e)}

    def summarize(self, result):
        with self.summarizers.reserve() as summarizer:
            summary = summarizer.parse(result)
            return summary

    async def ObtainSummary(self, url):
        result = await asyncio.wrap_future(self.executor.submit(self.do_work, url))
        if "error" in result:
            await self.channel.basic_publish(
                exchange="",
                routing_key="news.extract_failures",
                body=json.dumps(result).encode("utf8"),
                properties=aiormq.spec.Basic.Properties(app_id="newsparser", content_type="application/json"),
            )
        else:
            result2 = await asyncio.wrap_future(self.executor.submit(self.summarize, result))
            if "error" in result2:
                await self.channel.basic_publish(
                    exchange="",
                    routing_key="news.extract_failures",
                    body=json.dumps(result2).encode("utf8"),
                    properties=aiormq.spec.Basic.Properties(app_id="newsparser", content_type="application/json"),
                )
            else:
                await self.channel.basic_publish(
                    exchange="",
                    routing_key="news.articles",
                    body=json.dumps(result2).encode("utf8"),
                    properties=aiormq.spec.Basic.Properties(app_id="newsparser", content_type="application/json"),
                )

    async def on_message(self, message: aiormq.types.DeliveredMessage):
        payload = json.loads(message.body)
        print(payload)
        await self.ObtainSummary(payload["url"])
        await message.channel.basic_ack(message.delivery.delivery_tag)

    async def from_queue(self):
        await self.channel.basic_qos(prefetch_count=50)

        # Declaring queue
        declare_ok = await self.channel.queue_declare("news.clean_urls", durable=True)

        # Start listening the queue with name 'task_queue'
        await self.channel.basic_consume(declare_ok.queue, self.on_message)

    async def done(self):
        await self.channel.close()
        await self.connection.close()


class OutputHandler:
    def __init__(self, summaries, fails, queue_url):
        self.summaries = summaries
        self.fails = fails
        self.url = queue_url

    async def flush_summaries(self):
        connection = await aiormq.connect(self.url)
        channel = await connection.channel()
        while True:
            summary = await self.summaries.get()
            if summary is None:
                break
            d = json.dumps(summary)
            print(d)
            await channel.basic_publish(
                exchange="",
                routing_key="news.articles",
                body=d.encode("utf8"),
                properties=aiormq.spec.Basic.Properties(app_id="newsparser", content_type="application/json"),
            )
            self.summaries.task_done()
        await channel.close()
        await connection.close()

    async def flush_fails(self):
        connection = await aiormq.connect(self.url)
        channel = await connection.channel()
        while True:
            fail = await self.fails.get()
            if fail is None:
                break
            d = json.dumps(fail)
            await channel.basic_publish(
                exchange="",
                routing_key="news.extract_failures",
                body=d.encode("utf8"),
                properties=aiormq.spec.Basic.Properties(app_id="newsparser", content_type="application/json"),
            )
            self.fails.task_done()
        await channel.close()
        await connection.close()


async def close_queues(queues):
    for queue in queues:
        await queue.put(None)


async def from_queue():
    Config = ConfigParser()
    Config.read("conf.ini")
    workers = Config.getint("Browser", "instances")
    grid_url = Config.get("Browser", "url")
    amqp_url = Config.get("AMQP", "url")
    print("Running with {} workers".format(workers))

    executor = concurrent.futures.ThreadPoolExecutor(max_workers=5 * workers)
    s = SummaryGenerator(executor, grid_url, workers)
    await s.connect(amqp_url)
    await s.from_queue()


def main():
    # Ah! uvloop goes brrr
    uvloop.install()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(from_queue())
    loop.run_forever()


if __name__ == "__main__":
    main()
