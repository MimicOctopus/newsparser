#!/usr/bin/env python3

from socket import gethostbyname
from validators.url import url as validate_url
from urllib.parse import urlparse


def _is_valid(url: str, check_dns: bool = True) -> bool:
    """
    Checks whether the provided url is valid
    :param url: The url to validate
    :param check_dns: Whether or not to validate the domain resolves in DNS
    """
    if validate_url(url):
        parsed_url = urlparse(url)
        host = parsed_url.netloc
        if not check_dns or (check_dns and gethostbyname(host)):
            return True
    return False


def filter_urls(urls: [str]) -> [str]:
    """
    Filter the given URL list down to a set of valid urls
    :param urls: A list of urls
    :return : A list of validated urls
    """
    return [url for url in urls if _is_valid(url)]


class URLManager(object):
    """A url manager"""

    def __init__(self) -> None:
        self._urls = set()

    def add_urls(self, urls: [str]) -> None:
        """
        Add the given list of URLS to this manager.
        URLs are validated before behing added.
        Invalid URLs are simply ignored.
        :param urls: URLs to be added
        """
        self._urls = self._urls.union(set(filter_urls(urls)))

    def urls(self) -> [str]:
        """
        Get the list of URL currently held by this manager
        :return : The list of URLs
        """
        return list(self._urls)

    def reset(self) -> None:
        """
        Reset the list of URLs
        """
        self._urls.clear()


class UrlFilter:
    def __init__(self) -> None:
        from fastBloomFilter import bloom

        self.bf = bloom.BloomFilter()

    def filter_urls(self, urls):
        for url in urls:
            if _is_valid(url) and not self.bf.update(url.encode("utf-8")):
                yield url
