#!/usr/bin/env python3

from newsparser.urlmanager import URLManager
from newsparser.urlutils import parse_html_for_urls
import mailbox
import quopri

URLFILE = "urls.txt"
MAILDIR = "~/Maildir"


class MailManager:
    def __init__(self, mdir, loader):
        self.mdir = mdir
        self.loader = loader

    def getPayloads(self, message):
        msg = message.get_payload()
        if message["Content-Transfer-Encoding"] == "quoted-printable":
            msg = str(quopri.decodestring(msg))
        if type(msg) is str:
            yield msg
        else:
            for m in msg:
                a = self.getPayloads(m)
                for f in a:
                    yield f

    def GetURLToSummarize(self):
        self.mdir.lock()
        for k, message in self.mdir.iteritems():
            for p in self.getPayloads(message):
                urls = parse_html_for_urls(p)
                self.loader.add_urls(urls)
            self.mdir.remove(k)
        self.mdir.unlock()
        return set(filter(None, self.loader.urls()))


if __name__ == "__main__":
    loader = URLManager()
    mdir = mailbox.Maildir(MAILDIR, factory=mailbox.MaildirMessage, create=True)
    mailMan = MailManager(mdir, loader)

    urltosummarize = mailMan.GetURLToSummarize()

    with open(URLFILE, "wb") as f:
        for url in urltosummarize:
            f.write(("%s\n" % url).encode("UTF8"))
