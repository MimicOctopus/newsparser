import feedparser
import asyncio
import aiormq
import sys
import uvloop
from configparser import RawConfigParser as ConfigParser


def get_feed_urls(feed):
    d = feedparser.parse(feed)
    links = []
    [links.extend(i["links"]) for i in d["entries"]]
    return set([e["href"] for e in links])


def get_feeds():
    for i in sys.stdin:
        i = i.strip()
        if i == "":
            continue
        yield i


async def handle_all_feeds(feeds):
    config = ConfigParser()
    config.read("conf.ini")
    amqp_url = config.get("AMQP", "url")
    connection = await aiormq.connect(amqp_url)
    channel = await connection.channel()
    jobs = []
    for feed in feeds:
        for url in get_feed_urls(feed):
            jobs.append(
                asyncio.create_task(
                    channel.basic_publish(
                        exchange="",
                        routing_key="news.new_urls",
                        body=url.encode("utf8"),
                        properties=aiormq.spec.Basic.Properties(app_id="newsparser", content_type="application/json"),
                    )
                )
            )
    await asyncio.gather(*jobs)
    await channel.close()
    await connection.close()


def main():
    # Ah! uvloop goes brrr
    uvloop.install()
    loop = loop = asyncio.get_event_loop()
    loop.run_until_complete(handle_all_feeds(get_feeds()))


if __name__ == "__main__":
    for feed in feeds:
        for url in get_feed_urls(feed):
            print(url)
