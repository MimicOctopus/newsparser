#!/usr/bin/env python3

import asyncio
import aiormq
import asyncpg
import json
import uuid
import uvloop
from collections import deque
from feedgen.feed import FeedGenerator
from sanic import Sanic
from sanic import response
from configparser import RawConfigParser as ConfigParser
from datetime import datetime, timezone


app = Sanic(__name__)


class NewsFeed:
    def __init__(self, url, database_uri, title, description, link):
        self.url = url
        self.db_uri = database_uri
        self.title = title
        self.description = description
        self.link = link
        

    async def on_message(self, message: aiormq.types.DeliveredMessage):
        payload = json.loads(message.body)
        async with self.pool.acquire() as pgconn:    
            print("Connected to DB")
            await pgconn.execute('''INSERT INTO summaries (url, title, summary, published) values ($1, $2, $3, $4) ON CONFLICT DO NOTHING''', payload["url"], payload["title"], payload["summary"], datetime.utcnow())
            print("Transaction commited")
            
    
    async def prep_db(self):
        # Much cleaning up needed here
        # Make enter/exit
        self.pool = await asyncpg.create_pool(dsn=self.db_uri)
        # Regular stuff goes here
        print("Doing some prep")
        async with self.pool.acquire() as pgconn:
            await pgconn.execute("CREATE TABLE IF NOT EXISTS summaries (url text UNIQUE, title text, summary text, published timestamp)")

    async def get_articles(self):
        # Perform connection
        self.connection = await aiormq.connect(self.url)

        # Creating a channel
        channel = await self.connection.channel()
        await channel.basic_qos(prefetch_count=1)

        # Declaring queue
        declare_ok = await channel.queue_declare("news.articles", durable=True)

        # Start listening the queue with name 'task_queue'
        print("Consuming")
        await channel.basic_consume(declare_ok.queue, self.on_message, no_ack=True)

    async def ipsum(self, request):
        async with self.pool.acquire() as pgconn:
            fg = FeedGenerator()
            fg.title(self.title)
            fg.link(href=self.link)
            fg.description(self.description)
            fg.language("en")
            values = await pgconn.fetch("SELECT * FROM summaries where published > current_date - interval '7' day")
            for i in values:
                fe = fg.add_entry()
                fe.guid(i["url"], permalink=True)
                fe.title(i["title"])
                fe.link(href=i["url"])
                fe.content(content=i["summary"])
                fe.pubDate(pubDate=i["published"].replace(tzinfo=timezone.utc))
            return response.text(fg.rss_str(pretty=True).decode("utf8"), content_type="application/rss+xml")

def main():
    # Ah! uvloop goes brrr
    uvloop.install()
    config = ConfigParser()
    config.read("conf.ini")
    amqp_url = config.get("AMQP", "url")
    database_uri = config.get("DB", "connect_uri")
    title = config.get("RSS", "title")
    description = config.get("RSS", "description")
    link = config.get("RSS", "link")
    nf = NewsFeed(amqp_url, database_uri, title, description, link)
    app.add_route(nf.ipsum, "/feed.rss")

    # Prep
    loop = asyncio.get_event_loop()
    loop.run_until_complete(nf.prep_db())

    # Run
    server = app.create_server(host="0.0.0.0", port=8000, debug=False, return_asyncio_server=True)
    messages = asyncio.ensure_future(nf.get_articles())
    task = asyncio.ensure_future(server)
    loop.run_forever()

if __name__ == "__main__":
    main()
