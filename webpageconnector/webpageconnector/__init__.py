#!/usr/bin/env python3

import asyncio
import aiormq
import sys
import requests
import uvloop
from configparser import RawConfigParser as ConfigParser
from bs4 import BeautifulSoup


def parse_html_for_urls(body: str) -> [str]:
    urls = set()
    soup = BeautifulSoup(body, "lxml")
    for link in soup.find_all("a", href=True):
        if None is link:
            continue
        a = link["href"]
        urls.add(a)
    return list(urls)


def get_urls():
    for i in sys.stdin:
        i = i.strip()
        if i == "":
            continue
        print("Crawling: " + i, file=sys.stderr)
        try:
            r = requests.get(i)
            status = r.status_code
            response = r.text
        except Exception as e:
            continue
        for url in parse_html_for_urls(response):
            yield url


async def handle_all_urls(urls):
    Config = ConfigParser()
    Config.read("conf.ini")
    amqp_url = Config.get("AMQP", "url")
    connection = await aiormq.connect(amqp_url)
    channel = await connection.channel()
    jobs = []
    for url in urls:
        jobs.append(
            asyncio.create_task(
                channel.basic_publish(
                    exchange="",
                    routing_key="news.new_urls",
                    body=url.encode("utf8"),
                    properties=aiormq.spec.Basic.Properties(app_id="newsparser", content_type="application/json"),
                )
            )
        )
    await asyncio.gather(*jobs)
    await channel.close()
    await connection.close()


def main():
    # Ah! uvloop goes brrr
    uvloop.install()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(handle_all_urls(get_urls()))


if __name__ == "__main__":
    main()
